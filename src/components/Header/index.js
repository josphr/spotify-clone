import React from 'react'
import TrackSearch from './TrackSearch'
import UserDetails from './UserDetails'

const Header = () => {
    return (
        <div>
            <TrackSearch />
            <UserDetails />
        </div>
    )
}

export default Header